# palette-gen

Generate a range of colors surrounding a single color in RGB space.

![Screenshot](screenshot.png)

![Example](example.png)
