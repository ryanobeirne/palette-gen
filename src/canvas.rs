use iced::{
    canvas::{
        self, Canvas, Cursor, Fill, Frame, Geometry, Path, Program, Stroke,
    },
    Point, Rectangle, Color, Size,
};
use super::{Chip, Message, ColorMap};

impl Program<Message> for ColorMap {
    fn draw(&self, bounds: Rectangle<f32>, cursor: Cursor) -> Vec<Geometry> {
        let Size {width, height} = bounds.size();
        let patch_size = Size {
            width: width / *self.patches_x() as f32,
            height: height / *self.patches_y() as f32,
        };

        let color_map = self.canvas().draw(bounds.size(), |frame| {
            let mut origin = Point::ORIGIN;
            let mut center = Path::rectangle(Point::ORIGIN, patch_size);
            frame.fill_rectangle(origin, bounds.size(), Color::from_rgb8(128, 128, 128));

            let mut x = 1;
            for patch in self.patches() {

                let rect = Path::rectangle(origin, patch_size);
                frame.fill(&rect, Color::from(patch));

                if patch == self.center_color() {
                    center = rect;
                }

                if x >= *self.patches_x() {
                    x = 1;
                    origin.x = 0.0;
                    origin.y += patch_size.height;
                } else {
                    origin.x += patch_size.width;
                    x += 1;
                }
            }

            frame.stroke(
                &center,
                Stroke {
                    width: 2.0,
                    color: invert(&Color::from(self.center_color())),
                    ..Stroke::default()
                }
            );
        });

        vec![color_map]
    }
}

impl Program<Message> for Chip {
    fn draw(&self, bounds: Rectangle<f32>, _cursor: Cursor) -> Vec<Geometry> {
        let chip = self.cache.draw(bounds.size(), |frame| {
            let rect = Path::rectangle(Point::ORIGIN, bounds.size());
            frame.fill(&rect, self.color);
            frame.stroke(&rect, Stroke { width: 1.0, ..Stroke::default() });
        });

        vec![chip]
    }
}

fn invert(color: &Color) -> Color {
    Color::from_rgb(
        1.0 - color.r,
        1.0 - color.g,
        1.0 - color.b,
    )
}
