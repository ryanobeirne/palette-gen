use crate::*;
use conv::ValueInto;
use hilbert::fast_hilbert::hilbert_axes;
use iced::canvas::Cache;
use image::{GenericImage, GenericImageView, Pixel, Rgb, RgbImage};
use imageproc::definitions::Clamp;
use imageproc::drawing::{draw_filled_rect_mut, draw_hollow_rect_mut, draw_text_mut, Canvas};
use imageproc::rect::Rect;
use num_bigint::BigUint;
use num_traits::ToPrimitive;
use rusttype::{Font, Point, Scale};
use std::collections::BTreeSet;

#[derive(Debug)]
pub struct ColorMap {
    center_color: ColorPatch,
    patches: BTreeSet<ColorPatch>,
    patch_size: Size,
    gap_size: Size,
    spread: u32,
    canvas: Cache, 
    patches_x: u32,
    patches_y: u32,
}

impl ColorMap {
    /// Create a new ColorMap
    pub fn new(center_color: ColorPatch, patch_size: Size, gap_size: Size, spread: u32) -> Self {
        ColorMap {
            center_color,
            patches: BTreeSet::new(),
            patch_size,
            gap_size,
            spread,
            canvas: Cache::new(),
            patches_x: 0,
            patches_y: 0,
        }
    }

    pub fn center_color(&self) -> &ColorPatch {
        &self.center_color
    }

    pub fn set_center_color(&mut self, color: ColorPatch) {
        self.center_color = color;
    }

    pub fn len(&self) -> usize {
        self.patches.len()
    }

    pub fn patches(&self) -> std::collections::btree_set::Iter<ColorPatch> {
        self.patches.iter()
    }

    pub fn patches_x(&self) -> &u32 {
        &self.patches_x
    }

    pub fn patches_y(&self) -> &u32 {
        &self.patches_y
    }

    pub fn spread(&self) -> &u32 {
        &self.spread
    }

    pub fn set_spread(&mut self, spread: u32) {
        self.spread = spread;
    }

    pub fn gap_size(&self) -> &Size {
        &self.gap_size
    }

    pub fn gap_size_x(&self) -> &u32 {
        &self.gap_size().x
    }

    pub fn set_gap_size(&mut self, size: Size) {
        self.gap_size = size
    }

    pub fn gap_size_y(&self) -> &u32 {
        &self.gap_size().y
    }

    pub fn set_patch_size(&mut self, size: Size) {
        self.patch_size = size;
    }

    pub fn patch_size_x(&self) -> &u32 {
        &self.patch_size.x
    }

    pub fn set_patch_size_x(&mut self, x: u32) {
        self.patch_size.x = x;
    }

    pub fn patch_size_y(&self) -> &u32 {
        &self.patch_size.y
    }

    pub fn set_patch_size_y(&mut self, y: u32) {
        self.patch_size.y = y;
    }

    pub fn canvas(&self) -> &Cache {
        &self.canvas
    }

    pub fn canvas_cache(&mut self) -> &mut Cache {
        &mut self.canvas
    }

    /// Insert a ColorPatch into a ColorMap
    pub fn insert(&mut self, color: ColorPatch) -> bool {
        self.patches.insert(color)
    }

    /// The maximum Hilbert index of an 8th order 3D Hilbert cube
    const MAX: i32 = 16777215;

    /// Populate a ColorMap with ColorPatches based on the center_color
    pub fn populate<G: GenericImageView>(&mut self, image: &G) {
        self.patches.clear();
        self.insert(self.center_color.clone());

        let max_patches = self.max_patches(image);

        let mut mid = self.center_color.hilbert_index().to_i32().unwrap();

        if self.spread == 0 {
            self.spread = 1;
        }

        let distance = max_patches as i32 * self.spread as i32;
        let half_distance = distance / 2;

        if half_distance > mid {
            mid = half_distance;
        }

        if Self::MAX - half_distance < mid {
            mid = Self::MAX - half_distance;
        };

        let mut left = mid - half_distance;

        if left + distance > Self::MAX {
            left = Self::MAX - distance;
        }

        let mut i = left;
        while self.patch_count() < max_patches {
            let rgb = hilbert_axes(&BigUint::from(i as usize), 8, 3);
            self.insert(ColorPatch::new(rgb[0] as u8, rgb[1] as u8, rgb[2] as u8));
            i += self.spread as i32;
        }
        
        let max_patches_xy = self.max_patches_xy(image);
        self.patches_x = max_patches_xy.x;
        self.patches_y = max_patches_xy.y;
    }

    /// Draw a ColorPatch to an Image
    pub fn draw_patch<'a, C>(&'a self, color: &ColorPatch, origin: Point<i32>, image: &mut C)
    where
        C: Canvas + Canvas<Pixel = Rgb<u8>> + GenericImage,
        <<C as Canvas>::Pixel as Pixel>::Subpixel: ValueInto<f32> + Clamp<f32>,
    {
        draw_filled_rect_mut(
            image,
            Rect::at(origin.x as i32, origin.y as i32)
                .of_size(self.patch_size.x, self.patch_size.y),
            *color.as_rgb(),
        );

        let text_pos = Point {
            x: origin.x,
            y: origin.y + self.patch_size.y as i32,
        };

        draw_text_mut(
            image,
            COLOR_TEXT,
            text_pos.x as u32,
            text_pos.y as u32,
            self.text_size(),
            &Font::try_from_bytes(FONT).expect("Font::try_from_bytes()"),
            &color.to_hex(),
        );
    }

    /// Draw all the ColorPatches to an Image
    pub fn draw_image(&self, image: &mut RgbImage) {
        let mut center_point = None;

        for (draw_point, patch) in self.draw_points(image).zip(self.patches.iter()) {
            if patch == &self.center_color {
                center_point = Some(draw_point);
            }
            self.draw_patch(patch, draw_point, image);
        }

        // Highlight the center patch
        if let Some(draw_point) = center_point {
            self.highlight_patch(draw_point, image);
        }

        // Outline the entire image
        for i in 0..(self.highlight_stroke_width()) {
            draw_hollow_rect_mut(
                image,
                Rect::at(i as i32, i as i32).of_size(image.width() - i * 2, image.height() - i * 2),
                COLOR_TEXT,
            );
        }
    }

    /// Like `draw()` but empties the canvas first
    pub fn update_image_buffer(&self, image: &mut RgbImage) {
        *image = RgbImage::from_pixel(image.width(), image.height(), COLOR_BG);
        self.draw_image(image);
    }

    /// Draw a rectangle around the center_color
    pub fn highlight_patch<'a, C>(&'a self, origin: Point<i32>, image: &mut C)
    where
        C: Canvas + Canvas<Pixel = Rgb<u8>> + GenericImage,
        <<C as Canvas>::Pixel as Pixel>::Subpixel: ValueInto<f32> + Clamp<f32>,
    {
        let outline_size = self.highlight_outline_size();

        for i in 1..self.highlight_stroke_width() {
            draw_hollow_rect_mut(
                image,
                Rect::at(origin.x - i as i32, origin.y - i as i32)
                    .of_size(outline_size.x + i * 2, outline_size.y + i * 2),
                COLOR_TEXT,
            );
        }
    }

    /// Calculate the stroke width for highlighting a patch
    fn highlight_stroke_width(&self) -> u32 {
        let half_gap = if self.gap_size_x() < self.gap_size_y() {
            self.gap_size_x() / 2
        } else {
            self.gap_size_y() / 2
        };

        if half_gap < 2 {
            2
        } else if half_gap > 10 {
            10
        } else {
            half_gap
        }
    }

    fn highlight_outline_size(&self) -> Size {
        Size::new(
            *self.patch_size_x(),
            self.patch_size_y() + self.text_size().y as u32,
        )
    }

    /// Iterate over patch origin points (top left) for drawing patches to an image
    pub fn draw_points<G: GenericImageView>(&self, image: &G) -> DrawPoints<i32> {
        let mut points = Vec::with_capacity(self.len());
        let Size { x: max_x, y: max_y } = self.max_patches_xy(image);

        let top_left = self.top_left_draw_point(image);
        let mut draw_point = top_left.clone();
        points.push(draw_point);

        let (right_edge_x, bot_edge_y, gap_x, gap_y) = (
            top_left.x + ((max_x * self.gap_x()) - self.gap_size.x) as i32,
            top_left.y + ((max_y * self.gap_y()) - self.gap_size.y) as i32,
            self.gap_x() as i32,
            self.gap_y() as i32,
        );

        for _patch in self.patches.iter() {
            draw_point.x += gap_x;

            if draw_point.x > right_edge_x {
                draw_point.y += gap_y;
                draw_point.x = top_left.x;
            }

            if draw_point.y > bot_edge_y {
                break;
            }

            points.push(draw_point);
        }

        points.into()
    }

    /// Find the origin for drawing the top left ColorPatch based on the patch size, patch gap,
    /// patch count, and image size
    pub fn top_left_draw_point<G: GenericImageView>(&self, image: &G) -> Point<i32> {
        let Size { x: max_x, y: max_y } = self.max_patches_xy(image);

        let gap_left = (image.width() - (max_x * self.gap_x())) / 2;
        let gap_top = (image.height() - (max_y * self.gap_y())) / 2;

        Point {
            x: gap_left as i32,
            y: gap_top as i32,
        }
    }

    /// Draw points in a spiral pattern starting in the center of the Image
    pub fn draw_points_spiral<G: GenericImageView>(&self, image: &G) -> DrawPoints<i32> {
        let mut points = Vec::with_capacity(self.len());

        let mut draw_point = self.center_draw_point(image);
        points.push(draw_point);

        let mut spiral = SpiralIter::new();
        while self.is_still_inside(&draw_point, image) {
            let (direction, length) = spiral.next().unwrap();

            for _ in 0..length {
                draw_point = self.next_spiral_point(&draw_point, &direction);
                if !self.patch_is_outside(&draw_point, image) {
                    points.push(draw_point);
                }
            }
        }

        points.into()
    }

    /// Find the top-left coordinates for the center patch on the canvas
    pub fn center_draw_point<G: GenericImageView>(&self, image: &G) -> Point<i32> {
        let image_center = Point {
            x: image.width() / 2,
            y: image.height() / 2,
        };

        let Size { x: max_x, y: max_y } = self.max_patches_xy(image);

        // if patch-count in width is even
        let x = if max_x % 2 == 0 {
            image_center.x - self.patch_size.x - (self.gap_size.x / 2)
        // or odd
        } else {
            image_center.x - (self.patch_size.x / 2)
        } as i32;

        // if patch-count in height is even
        let y = if max_y % 2 == 0 {
            image_center.y - self.patch_size.y - self.text_size().y as u32 - (self.gap_size.y / 2)
        // or odd
        } else {
            image_center.y - ((self.patch_size.y + self.text_size().y as u32) / 2)
        } as i32;

        Point { x, y }
    }

    /// Test if a draw point will be totally inside the boundaries of an image
    pub fn is_still_inside<G: GenericImageView>(&self, draw_point: &Point<i32>, image: &G) -> bool {
        (draw_point.x > self.gap_size.x as i32 && draw_point.x < image.width() as i32)
            || (draw_point.y > self.gap_size.y as i32 && draw_point.y < image.height() as i32)
    }

    /// Find the origin of the next ColorPatch in a spiral draw pattern
    pub fn next_spiral_point(&self, point: &Point<i32>, direction: &Direction) -> Point<i32> {
        let x = match direction {
            Right => point.x + self.gap_x() as i32,
            Left => point.x - self.gap_x() as i32,
            Down | Up => point.x,
        };

        let y = match direction {
            Right | Left => point.y,
            Down => point.y + self.gap_y() as i32,
            Up => point.y - self.gap_y() as i32,
        };

        Point { x, y }
    }

    /// Test if a given Point is still inside the boundaries of an Image
    pub fn patch_is_outside<G: GenericImageView>(&self, point: &Point<i32>, image: &G) -> bool {
        point.x > self.edge_x(image)
            || point.x < self.gap_size.x as i32
            || point.y > self.edge_y(image)
            || point.y < self.gap_size.y as i32
    }

    /// The horizontal gap between patch origin points
    pub fn gap_x(&self) -> u32 {
        self.patch_size.x + self.gap_size.x
    }

    /// The vertical gap between patch origin points
    pub fn gap_y(&self) -> u32 {
        self.patch_size.y + self.text_size().y as u32 + self.gap_size.y
    }

    /// Maximum horizontal pixel to start drawing a patch
    pub fn edge_x<G: GenericImageView>(&self, image: &G) -> i32 {
        (image.width() - self.gap_x()) as i32
    }

    /// Maximum vertical pixel to start drawing a patch
    pub fn edge_y<G: GenericImageView>(&self, image: &G) -> i32 {
        (image.height() - self.gap_y()) as i32
    }

    /// The size of the label text. Proportional to the patch size.
    pub fn text_size(&self) -> Scale {
        let size = Scale::uniform(self.patch_size.x as f32 / 4.0);
        if size.x > 36.0 {
            Scale::uniform(36.0)
        } else {
            size
        }
    }

    /// Set the patch size for the ColorMap
    pub fn with_patch_size(mut self, patch_size: Size) -> Self {
        self.patch_size = patch_size;
        self
    }

    /// Set the gap size for the ColorMap
    pub fn with_gap_size(mut self, gap_size: Size) -> Self {
        self.gap_size = gap_size;
        self
    }

    /// Returns true if there are more patches in the ColorMap than the Image can hold.
    pub fn will_overflow<G: GenericImageView>(&self, image: &G) -> bool {
        self.patches.len() > self.max_patches(image)
    }

    /// Calculate the maxmimum number of patches in each direction that an image can hold based on
    /// the size of the patches in the ColorMap
    pub fn max_patches_xy<G: GenericImageView>(&self, image: &G) -> Size {
        Size {
            x: (image.width() - self.gap_size.x) / (self.patch_size.x + self.gap_size.x),
            y: (image.height() - self.gap_size.y)
                / (self.patch_size.y + self.text_size().x as u32 + self.gap_size.y),
        }
    }

    /// Calculate the maxmimum number of patches that an image can hold based on
    /// the size of the patches in the ColorMap
    pub fn max_patches<G: GenericImageView>(&self, image: &G) -> usize {
        let Size { x, y } = self.max_patches_xy(image);

        (x * y) as usize
    }

    /// Returns the number of patches in a ColorMap
    pub fn patch_count(&self) -> usize {
        self.patches.len()
    }
}

impl Default for ColorMap {
    fn default() -> Self {
        ColorMap {
            center_color: ColorPatch::new(0xff, 0xff, 0xff),
            patch_size: Size::square(150),
            gap_size: Size::square(10),
            spread: 10,
            patches: BTreeSet::new(),
            canvas: Cache::new(),
            patches_x: 0,
            patches_y: 0,
        }
    }
}
