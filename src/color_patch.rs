use crate::utils::ToHex;
use hilbert::fast_hilbert::hilbert_index;
use image::Rgb;
use num_bigint::BigUint;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ColorPatch {
    rgb: Rgb<u8>,
    hilbert_index: BigUint,
}

impl ColorPatch {
    /// Create a new ColorPatch from RGB u8's
    pub fn new(r: u8, g: u8, b: u8) -> Self {
        ColorPatch {
            rgb: Rgb([r, g, b]),
            hilbert_index: hilbert_index(&[r as u32, g as u32, b as u32], 8, None),
        }
    }

    /// Returns a reference to the inner RGB<u8>
    pub fn as_rgb(&self) -> &Rgb<u8> {
        &self.rgb
    }

    /// Returns a reference to the red value
    pub fn red(&self) -> &u8 {
        &self.rgb[0]
    }

    pub fn set_red(&mut self, red: u8) {
        self.rgb[0] = red;
    }

    /// Returns a reference to the green value
    pub fn green(&self) -> &u8 {
        &self.rgb[1]
    }

    pub fn set_green(&mut self, green: u8) {
        self.rgb[1] = green;
    }

    /// Returns a reference to the blue value
    pub fn blue(&self) -> &u8 {
        &self.rgb[2]
    }

    pub fn set_blue(&mut self, blue: u8) {
        self.rgb[2] = blue;
    }

    /// Returns an array of u32: `[R, G, B]`
    pub fn as_array(&self) -> [u32; 3] {
        self.into()
    }

    pub fn hilbert_index(&self) -> &BigUint {
        &self.hilbert_index
    }
}

impl<T: From<u8>> From<&ColorPatch> for [T; 3] {
    fn from(color_patch: &ColorPatch) -> Self {
        [
            (*color_patch.red()).into(),
            (*color_patch.green()).into(),
            (*color_patch.blue()).into(),
        ]
    }
}

impl From<ColorPatch> for iced::Color {
    fn from(color_patch: ColorPatch) -> Self {
        iced::Color::from_rgb8(
            *color_patch.red(),
            *color_patch.green(),
            *color_patch.blue(),
        )
    }
}

impl From<&ColorPatch> for iced::Color {
    fn from(color_patch: &ColorPatch) -> Self {
        iced::Color::from_rgb8(
            *color_patch.red(),
            *color_patch.green(),
            *color_patch.blue(),
        )
    }
}

impl PartialOrd for ColorPatch {
    /// Orders ColorPatches by their Hilbert Indexes
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.hilbert_index.partial_cmp(&other.hilbert_index)
    }
}

impl Ord for ColorPatch {
    /// Orders ColorPatches by their Hilbert Indexes
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl fmt::Display for ColorPatch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.to_hex())
    }
}

const HEX: &str = "0123456789AaBbCcDdEeFf";

impl FromStr for ColorPatch {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let trimmed = s.trim().trim_start_matches('#');
        if trimmed.len() == 6 {
            for c in trimmed.chars() {
                if !HEX.contains(c) {
                    return Err("Invalid character");
                }
            }

            let mut red = String::new();
            let mut blue = String::new();
            let mut green = String::new();

            let mut chars = trimmed.chars();
            red.push(chars.next().unwrap());
            red.push(chars.next().unwrap());
            green.push(chars.next().unwrap());
            green.push(chars.next().unwrap());
            blue.push(chars.next().unwrap());
            blue.push(chars.next().unwrap());

            Ok(ColorPatch::new(
                u8::from_str_radix(&red, 16).unwrap(),
                u8::from_str_radix(&green, 16).unwrap(),
                u8::from_str_radix(&blue, 16).unwrap(),
            ))
        } else {
            Err("Invalid string length")
        }
    }
}

#[test]
fn parse_color_patch() {
    // Good
    assert!("#db8026".parse::<ColorPatch>().is_ok());
    assert!("#000000".parse::<ColorPatch>().is_ok());
    assert!("#ffffff".parse::<ColorPatch>().is_ok());
    assert!("de8c2b".parse::<ColorPatch>().is_ok());
    assert!("808080".parse::<ColorPatch>().is_ok());
    assert!("FFFFFF".parse::<ColorPatch>().is_ok());

    // Bad
    assert!("#fffffg".parse::<ColorPatch>().is_err());
    assert!("#00000g".parse::<ColorPatch>().is_err());
    assert!("#0000000".parse::<ColorPatch>().is_err());
    assert!("#0".parse::<ColorPatch>().is_err());
    assert!("#".parse::<ColorPatch>().is_err());
    assert!("".parse::<ColorPatch>().is_err());
    assert!("0".parse::<ColorPatch>().is_err());
}
