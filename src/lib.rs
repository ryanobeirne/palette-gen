mod color_map;
mod color_patch;
mod points;
mod spiral;
mod utils;

pub use color_map::*;
pub use color_patch::*;
pub use points::*;
pub use spiral::*;
pub use utils::*;

use image::Rgb;

pub const FONT: &[u8] = include_bytes!("Lato-Bold.ttf");
pub const COLOR_TEXT: Rgb<u8> = Rgb([0x0, 0x0, 0x0]);
pub const COLOR_BG: Rgb<u8> = Rgb([0xff, 0xff, 0xff]);
