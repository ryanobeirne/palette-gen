use iced::{
    button,
    canvas::{Cache, Canvas},
    executor, scrollable, slider, text_input,
    window::Settings as Window,
    Align, Application, Button, Color, Column, Command, Container, Element, Length, Row, Settings,
    Slider, Text, TextInput,
};
use image::{ImageBuffer, RgbImage};
use palette_gen::*;
use std::env;
use std::path::{PathBuf, Path};

mod style;
mod canvas;

type BoxErr = Box<dyn std::error::Error>;
type Result<T> = std::result::Result<T, BoxErr>;

fn main() -> Result<()> {
    //main_cli()
    PaletteGen::run(Settings {
        antialiasing: true,
        window: Window {
            size: (1200, 1000),
            min_size: Some((600, 600)),
            ..Window::default()
        },
        ..Settings::default()
    })?;
    Ok(())
}

#[derive(Debug, Default)]
struct Chip {
    cache: Cache,
    color: Color,
}

#[derive(Debug, Default)]
struct Slide {
    view: String,
    value: f32,
    slider: slider::State,
    input: text_input::State,
}

impl Slide {
    fn new(value: f32) -> Self {
        Slide {
            view: value.to_string(),
            value,
            ..Default::default()
        }
    }

    fn update(&mut self, value: f32) {
        self.view = value.to_string();
        self.value = value;
    }

    fn view<T, S>(&mut self, text_msg: T, slide_msg: S) -> (TextInput<Message>, Slider<f32, Message>)
    where
        T: 'static + Fn(String) -> Message,
        S: 'static + Fn(f32) -> Message,
    {
        (
            TextInput::new(
                &mut self.input,
                "0-255",
                &self.view,
                text_msg,
            )
            .width(Length::Units(40))
            .style(style::TextInputStyle),

            Slider::new(
                &mut self.slider,
                0.0..=255.0,
                self.value,
                slide_msg,
            )
        )
    }
}

#[derive(Debug)]
pub struct PaletteGen {
    color_map: ColorMap,
    image_buffer: RgbImage,
    chip: Chip,

    scroll: scrollable::State,
    update: button::State,
    save: button::State,
    save_path: text_input::State,
    save_path_value: String,

    text_color: String,
    input_text_color: text_input::State,

    red: Slide,
    green: Slide,
    blue: Slide,

    spread: Slide,

    image_width_value: String,
    image_width: text_input::State,
    image_height_value: String,
    image_height: text_input::State,

    patch_width_value: String,
    patch_width: text_input::State,
    patch_height_value: String,
    patch_height: text_input::State,

    gap_width_value: String,
    gap_width: text_input::State,
    gap_height_value: String,
    gap_height: text_input::State,
}

impl Default for PaletteGen {
    fn default() -> Self {
        // Fat Quarter size with white background
        let image_buffer = ImageBuffer::from_pixel(150 * 21, 150 * 18, COLOR_BG);

        let mut color_map = ColorMap::default();
        color_map.populate(&image_buffer);

        let mut palette_gen = PaletteGen {
            scroll: scrollable::State::default(),
            update: button::State::default(),
            save: button::State::default(),
            save_path: text_input::State::default(),
            save_path_value: String::default(),

            text_color: color_map.center_color().to_hex(),
            input_text_color: text_input::State::default(),

            red: Slide::new(*color_map.center_color().red() as f32),
            green: Slide::new(*color_map.center_color().green() as f32),
            blue: Slide::new(*color_map.center_color().blue() as f32),

            spread: Slide::new(*color_map.spread() as f32),

            image_width_value: image_buffer.width().to_string(),
            image_width: text_input::State::default(),
            image_height_value: image_buffer.height().to_string(),
            image_height: text_input::State::default(),

            patch_width_value: color_map.patch_size_x().to_string(),
            patch_width: text_input::State::default(),
            patch_height_value: color_map.patch_size_y().to_string(),
            patch_height: text_input::State::default(),

            gap_width_value: color_map.gap_size_x().to_string(),
            gap_width: text_input::State::default(),
            gap_height_value: color_map.gap_size_y().to_string(),
            gap_height: text_input::State::default(),

            color_map,
            image_buffer: image_buffer.clone(),
            chip: Chip::default(),
        };

        palette_gen.update_image(image_buffer);
        palette_gen
    }
}

use Message::*;
#[derive(Debug, Clone)]
pub enum Message {
    UpdateImageBuffer(RgbImage),
    UpdateImageWidth(String),
    UpdateImageHeight(String),
    UpdatePatchWidth(String),
    UpdatePatchHeight(String),
    UpdateGapWidth(String),
    UpdateGapHeight(String),
    UpdateColor(String),
    UpdateRed(String),
    UpdateRedSlider(f32),
    UpdateGreen(String),
    UpdateGreenSlider(f32),
    UpdateBlue(String),
    UpdateBlueSlider(f32),
    UpdateSpread(String),
    UpdateSpreadSlider(f32),
    UpdateSavePath(String),
    Save(String),
    ReDraw,
}

impl Application for PaletteGen {
    type Message = Message;
    type Flags = ();
    type Executor = executor::Default;

    fn title(&self) -> String {
        "palette-gen".to_string()
    }

    fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {
        (PaletteGen::default(), Command::none())
    }

fn view(&mut self) -> Element<Self::Message> {
        let (red_input, red_slider) = self.red.view(UpdateRed, UpdateRedSlider);
        let (green_input, green_slider) = self.green.view(UpdateGreen, UpdateGreenSlider);
        let (blue_input, blue_slider) = self.blue.view(UpdateBlue, UpdateBlueSlider);

        let color = style::row()
            // Hex color
            .push(style::text("Center Color:"))
            .push(
                Canvas::new(&mut self.chip)
                    .width(Length::Units(20))
                    .height(Length::Units(20))
            )
            .push(
                TextInput::new(
                    &mut self.input_text_color,
                    "#000000",
                    &self.text_color,
                    UpdateColor,
                )
                .width(Length::Units(90))
                .style(style::TextInputStyle),
            )
            // Red
            .push(style::text("R:"))
            .push(red_input)
            .push(red_slider)
            // Green
            .push(style::text("G:"))
            .push(green_input)
            .push(green_slider)
            // Blue
            .push(style::text("B:"))
            .push(blue_input)
            .push(blue_slider);

        // let (spread_input, spread_slider) = self.spread.view(UpdateSpread, UpdateSpreadSlider);
        let spread_input = TextInput::new(
            &mut self.spread.input,
            "0-1000",
            &self.spread.view,
            UpdateSpread,
        ).style(style::TextInputStyle);

        let spread_slider = Slider::new(
                &mut self.spread.slider,
                0.0..=1000.0,
                self.spread.value,
                UpdateSpreadSlider,
        );

        let spread = style::row()
            .push(style::text("Spread:"))
            .push(spread_input.width(Length::Units(100)))
            .push(spread_slider);

        let image_size = style::row()
            .push(style::text("Image Width:"))
            .push(
                TextInput::new(
                    &mut self.image_width,
                    "width (px)",
                    &self.image_width_value,
                    UpdateImageWidth,
                )
                .style(style::TextInputStyle)
                .on_submit(ReDraw)
                .width(Length::Units(100)),
            )
            .push(style::text("Height:"))
            .push(
                TextInput::new(
                    &mut self.image_height,
                    "height (px)",
                    &self.image_height_value,
                    UpdateImageHeight,
                )
                .style(style::TextInputStyle)
                .on_submit(ReDraw)
                .width(Length::Units(100)),
            )
            .push(style::text(&format!(
                "{:0.2}\" x {:0.2}\" @ 150 PPI",
                self.image_width_value.parse::<f32>().unwrap_or(0.0) / 150.0,
                self.image_height_value.parse::<f32>().unwrap_or(0.0) / 150.0,
            )));

        let patch_size = style::row()
            .push(style::text("Patch Width:"))
            .push(
                TextInput::new(
                    &mut self.patch_width,
                    "width (px)",
                    &self.patch_width_value,
                    UpdatePatchWidth,
                )
                .style(style::TextInputStyle)
                .on_submit(ReDraw)
                .width(Length::Units(100)),
            )
            .push(style::text("Height"))
            .push(
                TextInput::new(
                    &mut self.patch_height,
                    "height (px)",
                    &self.patch_height_value,
                    UpdatePatchHeight,
                )
                .style(style::TextInputStyle)
                .on_submit(ReDraw)
                .width(Length::Units(100)),
            )
            .push(style::text(&format!(
                "{:0.2}\" x {:0.2}\"",
                self.patch_width_value.parse::<f32>().unwrap_or(0.0) / 150.0,
                self.patch_height_value.parse::<f32>().unwrap_or(0.0) / 150.0,
            )))
            .push(style::text(&format!("({}x{}: {} patches)", self.color_map.patches_x(), self.color_map.patches_y(), self.color_map.len())));

        let gap_size = style::row()
            .push(style::text("Gap Width:"))
            .push(
                TextInput::new(
                    &mut self.gap_width,
                    "width (px)",
                    &self.gap_width_value,
                    UpdateGapWidth,
                )
                .style(style::TextInputStyle)
                .on_submit(ReDraw)
                .width(Length::Units(100)),
            )
            .push(style::text("Height:"))
            .push(
                TextInput::new(
                    &mut self.gap_height,
                    "height (px)",
                    &self.gap_height_value,
                    UpdateGapHeight,
                )
                .style(style::TextInputStyle)
                .on_submit(ReDraw)
                .width(Length::Units(100)),
            )
            .push(style::text(&format!(
                "{:0.2}\" x {:0.2}\"",
                self.gap_width_value.parse::<f32>().unwrap_or(0.0) / 150.0,
                self.gap_height_value.parse::<f32>().unwrap_or(0.0) / 150.0,
            )));

        let update = style::row()
            .push(Button::new(&mut self.update, style::text("Update")).on_press(ReDraw))
            .push(
                Button::new(&mut self.save, style::text("Save"))
                    .on_press(Save(self.save_path_value.clone())),
            )
            .push(
                TextInput::new(
                    &mut self.save_path,
                    "path/to/save.png",
                    &self.save_path_value,
                    UpdateSavePath,
                )
                .style(style::TextInputStyle)
                .padding(5)
                .on_submit(Save(self.save_path_value.clone())),
            );

        let controls = Column::new()
                .push(color)
                .push(spread)
                .push(image_size)
                .push(patch_size)
                .push(gap_size)
                .push(update);

        let canvas = Canvas::new(&mut self.color_map)
            .height(Length::Units(self.image_buffer.height() as u16))
            .width(Length::Units(self.image_buffer.width() as u16));

        //let scrollable = Scrollable::new(scroll)
            //.push(canvas);

        let content = Column::new()
            .push(controls)
            .push(canvas)
            .padding(20);

        Container::new(content)
            //.width(Length::Fill))
            //.height(Length::Fill)
            .center_x()
            .into()
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            UpdateImageBuffer(image) => self.update_image(image),
            UpdateColor(color) => self.update_color(color),
            UpdateRed(color) => self.update_channel(color, Red),
            UpdateRedSlider(color) => self.update_channel(color.to_string(), Red),
            UpdateGreen(color) => self.update_channel(color, Green),
            UpdateGreenSlider(color) => self.update_channel(color.to_string(), Green),
            UpdateBlue(color) => self.update_channel(color, Blue),
            UpdateBlueSlider(color) => self.update_channel(color.to_string(), Blue),
            UpdateSpread(spread) => self.update_spread(spread),
            UpdateSpreadSlider(spread) => self.update_spread(spread.to_string()),
            UpdateImageWidth(size) => self.update_image_size(size, Width),
            UpdateImageHeight(size) => self.update_image_size(size, Height),
            UpdateGapWidth(size) => self.update_gap_size(size, Width),
            UpdateGapHeight(size) => self.update_gap_size(size, Height),
            UpdatePatchWidth(size) => self.update_patch_size(size, Width),
            UpdatePatchHeight(size) => self.update_patch_size(size, Height),
            UpdateSavePath(path) => self.update_save_path(path),
            Save(path) => self.save(path),
            ReDraw => self.redraw(),
        }

        Command::none()
    }
}

impl PaletteGen {
    fn save<P: AsRef<Path>>(&mut self, path: P) {
        self.color_map.update_image_buffer(&mut self.image_buffer);
        let path = path.as_ref().clone().display().to_string().trim().to_string();

        // Use home directory alias '~/'
        let trim_path = path.trim_start_matches("~/").to_string();
        let new_path = if path != trim_path {
            let mut new_path = PathBuf::new();
            if let Ok(home) = env::var("HOME") {
                new_path.push(home);
                new_path.push(trim_path);
                new_path
            } else {
                PathBuf::from(path)
            }
        } else {
            PathBuf::from(path)
        };

        match self.image_buffer.save(&new_path) {
            Ok(_) => eprintln!("Saved image: '{}'", new_path.display()),
            Err(e) => eprintln!("'{}': {}", new_path.display(), e),
        }
    }

    fn update_save_path<P: AsRef<Path>>(&mut self, path: P) {
        self.save_path_value = path.as_ref().display().to_string();
    }

    fn update_image(&mut self, image: RgbImage) {
        self.image_buffer = image;
    }

    fn update_image_size(&mut self, size: String, wh: WidthHeight) {
        match wh {
            Width => {
                self.image_width_value = size.clone();
            }
            Height => {
                self.image_height_value = size.clone();
            }
        }

        if let Ok(uint) = size.parse::<u32>() {
            // If it's too small, or too big, return early
            if uint < self.color_map.gap_x() * 2 || uint > 18000 {
                return;
            }

            match wh {
                Width => {
                    self.image_buffer =
                        RgbImage::from_pixel(uint, self.image_buffer.height(), COLOR_BG);
                    self.image_width_value = size;
                }
                Height => {
                    self.image_buffer =
                        RgbImage::from_pixel(self.image_buffer.width(), uint, COLOR_BG);
                    self.image_height_value = size;
                }
            }

            self.redraw();
        }
    }

    fn update_patch_size(&mut self, size: String, wh: WidthHeight) {
        match wh {
            Width => self.patch_width_value = size.clone(),
            Height => self.patch_height_value = size.clone(),
        }

        if let Ok(uint) = size.parse::<u32>() {
            if uint < 75 {
                return;
            }

            match wh {
                Width => self.color_map.set_patch_size_x(uint),
                Height => self.color_map.set_patch_size_y(uint),
            }

            self.redraw();
        }
    }

    fn update_gap_size(&mut self, size: String, wh: WidthHeight) {
        match wh {
            Width => self.gap_width_value = size.clone(),
            Height => self.gap_height_value = size.clone(),
        }

        if let Ok(uint) = size.parse::<u32>() {
            // If it's too small, or too big, return early
            if &uint > self.color_map.patch_size_x() || &uint > self.color_map.patch_size_y() {
                return;
            }

            match wh {
                Width => {
                    self.color_map.set_gap_size(Size {
                        x: uint,
                        y: *self.color_map.gap_size_y(),
                    });
                    self.gap_width_value = size;
                }
                Height => {
                    self.color_map.set_gap_size(Size {
                        x: *self.color_map.gap_size_x(),
                        y: uint,
                    });
                }
            }

            self.redraw();
        }
    }

    fn set_center_color(&mut self, color: ColorPatch) {
        self.chip.color = (&color).into();
        self.color_map.set_center_color(color);
    }

    /// Parse a hex code and update the color map
    fn update_color(&mut self, color: String) {
        if let Ok(color) = color.parse::<ColorPatch>() {
            self.red.update(*color.red() as f32);
            self.green.update(*color.green() as f32);
            self.blue.update(*color.blue() as f32);
            self.text_color = color.to_string();
            if &color == self.color_map.center_color() {
                return;
            }
            self.set_center_color(color);
            self.redraw()
        } else {
            self.text_color = color;
        }
    }

    fn update_channel(&mut self, s: String, channel: RedGreenBlue) {
        if let Ok(color) = s.parse::<f32>() {
            let color = color.round();
            let mut center_color = self.color_map.center_color().clone();

            match channel {
                Red => {
                    center_color.set_red(color as u8);
                    self.red.view = color.to_string();
                    self.red.value = color;
                }
                Green => {
                    center_color.set_green(color as u8);
                    self.green.view = color.to_string();
                    self.green.value = color;
                }
                Blue => {
                    center_color.set_blue(color as u8);
                    self.blue.view = color.to_string();
                    self.blue.value = color;
                }
            }

            self.update_color(center_color.to_hex());
        } else {
            match channel {
                Red => self.red.view = s,
                Green => self.green.view = s,
                Blue => self.blue.view = s,
            }
        }
    }

    fn update_spread(&mut self, spread: String) {
        if let Ok(spread) = spread.parse::<f32>() {
            self.spread.update(spread);
            self.color_map.set_spread(spread.round() as u32);
            self.redraw();
        } else {
            self.spread.view = spread;
        }
    }

    fn redraw(&mut self) {
        self.set_center_color(
            self.text_color
                .parse()
                .unwrap_or(self.color_map.center_color().clone()),
        );

        self.chip.cache.clear();

        self.color_map.canvas_cache().clear();
        self.color_map.populate(&self.image_buffer);
        //self.color_map.update_image_buffer(&mut self.image_buffer);
        //self.png_encode().unwrap();
    }
}

fn main_cli() {
    // ouput file
    let arg = env::args().skip(1).nth(0).expect("nth()");
    let path = Path::new(&arg);

    // The patch color
    let patch_color = ColorPatch::new(0xd8, 0xd0, 0xbb);

    // The color map
    let mut color_map = ColorMap::new(patch_color, Size::square(150), Size::square(10), 1);

    // white background, 42x36" at 150 PPI
    let mut image = ImageBuffer::from_pixel(150 * 42, 150 * 36, COLOR_BG);

    color_map.populate(&image);

    color_map.draw_image(&mut image);

    image.save(path).expect("image.save()");
}
