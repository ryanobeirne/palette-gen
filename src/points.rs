use rusttype::Point;

/// An iterator over origin points (top left) for drawing patches to an image
#[derive(Debug)]
pub struct DrawPoints<T> {
    points: Vec<Point<T>>,
    index: usize,
}

impl<T: Copy> Iterator for DrawPoints<T> {
    type Item = Point<T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.points.len() {
            None
        } else {
            self.index += 1;
            Some(self.points[self.index - 1])
        }
    }
}

impl<T> From<Vec<Point<T>>> for DrawPoints<T> {
    fn from(points: Vec<Point<T>>) -> Self {
        DrawPoints { points, index: 0 }
    }
}
