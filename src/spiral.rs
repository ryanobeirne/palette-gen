pub use Direction::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
/// The 4 cardinal directions in 2D space
pub enum Direction {
    Right,
    Down,
    Left,
    Up,
}

impl Direction {
    /// Determine the next direction in a clockwise spiral
    fn next_direction(&self) -> Direction {
        match self {
            Direction::Right => Direction::Down,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Up => Direction::Right,
        }
    }

    /// Maps a direction to its gravity
    pub fn gravity(&self) -> Gravity {
        self.into()
    }
}

impl From<&Direction> for Gravity {
    fn from(direction: &Direction) -> Self {
        match direction {
            Direction::Left | Direction::Right => Gravity::Horizontal,
            Direction::Up | Direction::Down => Gravity::Vertical,
        }
    }
}

/// An iterator over the points in a square clockwise spiral
pub struct SpiralIter {
    direction: Direction,
    current_length: usize,
    last_length: usize,
}

impl SpiralIter {
    /// Constructs a new SpiralIter starting in the center
    pub fn new() -> Self {
        SpiralIter::default()
    }
}

impl Default for SpiralIter {
    fn default() -> Self {
        SpiralIter {
            direction: Direction::Right,
            current_length: 1,
            last_length: 0,
        }
    }
}

impl Iterator for SpiralIter {
    type Item = (Direction, usize);
    fn next(&mut self) -> Option<Self::Item> {
        let next = Some((self.direction, self.current_length));

        self.direction = self.direction.next_direction();

        if self.current_length == self.last_length {
            self.current_length += 1;
        } else {
            self.last_length = self.current_length;
        }

        next
    }
}

#[test]
fn spiral_iter() {
    let mut spiral = SpiralIter::new();
    assert_eq!(spiral.next(), Some((Direction::Right, 1)));
    assert_eq!(spiral.next(), Some((Direction::Down, 1)));
    assert_eq!(spiral.next(), Some((Direction::Left, 2)));
    assert_eq!(spiral.next(), Some((Direction::Up, 2)));
    assert_eq!(spiral.next(), Some((Direction::Right, 3)));
    assert_eq!(spiral.next(), Some((Direction::Down, 3)));
    assert_eq!(spiral.next(), Some((Direction::Left, 4)));
    assert_eq!(spiral.next(), Some((Direction::Up, 4)));
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
/// The gravity of a Direction
pub enum Gravity {
    Horizontal,
    Vertical,
}
