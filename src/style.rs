use super::*;
use iced::{text_input::Style, Color, VerticalAlignment as Vert};

pub struct TextInputStyle;

impl text_input::StyleSheet for TextInputStyle {
    fn active(&self) -> Style {
        Style {
            border_width: 1.0,
            border_color: Color::from_rgba8(0, 0, 0, 0.5),
            background: iced::Background::Color(Color::from_rgba8(0, 0, 0, 0.25)),
            border_radius: 0.0,
        }
    }

    fn focused(&self) -> Style {
        self.active()
    }

    fn placeholder_color(&self) -> Color {
        Color::from_rgba8(0, 0, 0, 0.25)
    }

    fn value_color(&self) -> Color {
        Color::BLACK
    }

    fn selection_color(&self) -> Color {
        Color::from_rgb8(0x47, 0xa8, 0xe4)
    }
}

pub fn text(text: &str) -> Text {
    Text::new(text).vertical_alignment(Vert::Center)
}

pub fn row<'a, T>() -> Row<'a, T> {
    Row::new().spacing(10).padding(5).align_items(Align::Start)
}
