use crate::ColorPatch;
use image::{Pixel, Rgb};

#[derive(Debug, Clone, Copy)]
pub struct Size {
    pub x: u32,
    pub y: u32,
}

impl Size {
    pub fn new(x: u32, y: u32) -> Self {
        Size { x, y }
    }

    pub fn square(size: u32) -> Self {
        Size { x: size, y: size }
    }
}

pub trait ToHex {
    fn to_hex(&self) -> String;
}

impl ToHex for Rgb<u8> {
    fn to_hex(&self) -> String {
        "#".chars()
            .chain(
                self.channels()
                    .iter()
                    .map(|u| format!("{:02X}", u))
                    .collect::<String>()
                    .chars(),
            )
            .collect()
    }
}

impl ToHex for ColorPatch {
    fn to_hex(&self) -> String {
        self.as_rgb().to_hex()
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Point3D {
    pub x: u8,
    pub y: u8,
    pub z: u8,
}

impl Point3D {
    pub fn new(x: u8, y: u8, z: u8) -> Self {
        Point3D { x, y, z }
    }
}

impl From<ColorPatch> for Point3D {
    fn from(patch: ColorPatch) -> Self {
        Point3D {
            x: *patch.red(),
            y: *patch.green(),
            z: *patch.blue(),
        }
    }
}

impl From<Point3D> for ColorPatch {
    fn from(point: Point3D) -> Self {
        ColorPatch::new(point.x, point.y, point.z)
    }
}

pub use Cardinal::*;
/// A value in a 3D direction
pub enum Cardinal {
    X(u8),
    Y(u8),
    Z(u8),
}

pub use RedGreenBlue::*;
#[derive(Debug, Copy, Clone)]
pub enum RedGreenBlue {
    Red,
    Green,
    Blue,
}

pub use WidthHeight::*;
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum WidthHeight {
    Width,
    Height,
}
